class AddDetailsToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :nombre, :string
    add_column :users, :documento, :float
    add_column :users, :fecha_nacimiento, :date
    add_column :users, :de_riesgo, :boolean
  end
end
