class HomeController < ApplicationController
  def index
    #listar datos de las instancias

  end
  
  def show
    #mostrar datos de una instancia
  
  end

  def new
    #mostrar formulario de carga(llama a ceate)
    
  end

  def edit
    #mostrar formulario de edicion(llama a update)
    
  end

  def create
    #crea en la BD
  end

  def update
    #modificar datos en la BD
    
  end

  def destroy
    #elimina en la BD
    
  end
end
